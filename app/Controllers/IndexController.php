<?php


namespace App\Controllers;


use http\Exception\RuntimeException;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Soen\Delay\Client\Client;
use Soen\Http\Message\HttpRequest;
use Soen\Log\Logger;
use Soen\Redis\Execute;
use Soen\Redis\Pool\Driver;
use Soen\Redis\RedisInterface;
use Soen\Redis\RedisProvider;

class IndexController
{
    /**
     * @return string
     * @throws \ReflectionException
     */
	function index(HttpRequest $request)
	{
        $j = 0;
        for ($i=0;$i<100000000;$i++){
            $j = $i*10;
        }
        /**
         * @var Driver $redis
         */
        $redis = \App::redis();
//        $redis = context()->getComponent('redis', true);
        \Swoole\Coroutine::sleep(1.5);
        $a = $request->get('a');
        $redis->set('name',$a);
        $name = $redis->get('name');
		echo \Swoole\Coroutine::getCid().PHP_EOL;
        var_dump($redis->getPool()->getConnCount());
        /**
         * @var Logger $log
         */
        $log = context()->getComponent('log', true);
        $log->warning(123123);
        return '这是redis值：'.$name;
	}

    /**
     * @return mixed
     * @throws \ReflectionException
     */
    function test1(HttpRequest $request)
    {
        $log = \App::log();
        $log->error('这是测试');
        $session = \App::session();
        $has = $session->set('name',$request->get('name', 'default——name'));
        return $has;
    }
    function test2()
    {
        $session = \App::session();
        $name = $session->get('name');
        $name = $session->get('age');
        return $name;
    }
    function test3()
    {
        $session = \App::session();
        $name = $session->set('age', ['a'=>'aaaaaa','b'=>'bbbbbbb']);
        return $name;
    }
    function test4(HttpRequest $request)
    {
//        $this->tt(1,2,3);
//        exit;
        $delayer = new Client();
        $delay = $request->get('delay');
        $content = $request->get('content');
        $id = $request->get('id');
        $topic = $request->get('topic');
        $body = ['content'=>$content];
        $delayer->push($id, $topic, $body, $delay);
    }

    function tt (...$a) {
        var_dump(implode(',', $a));
    }

	function test(HttpRequest $request)
	{
	    foo();
//	    set_error_handler(function (){
//	        echo '这是 一个注册 error';
//        });
        // 默认的日期格式是 "Y-m-d H:i:s"
//        $dateFormat = "Y-m-d H:i:s";
        // 默认的输出格式是 "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
//        $output = "%datetime% > %level_name% > %message% %context% %extra%\n";
        // 最后创建一个格式化器
//        $formatter = new LineFormatter($output, $dateFormat);
//        $logger = new Logger('app_log');
        // 创建一个处理器
//        $stream  = new StreamHandler('my_app.log', Logger::DEBUG);
//        $stream->setFormatter($formatter);
//        $logger->pushHandler($stream);
//        $logger->info('My logger is now ready', ['name'=>'soen', 'age'=>54]);
		return $request->get('id') . '--' . $request->get('name');
	}
}
<?php


namespace App\Listeners;


use Soen\Delay\Event\DelayExecute;
use Soen\Event\EventInterface;
use Soen\Event\ListenerInterface;

class DelayListener implements ListenerInterface
{
    public function events ():array {
        return [
            DelayExecute::class
        ];
    }

    public function process (EventInterface $event) {
        if($event instanceof DelayExecute){
            var_dump($event->job);
        }
    }
}
<?php


namespace App\Middlewares;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Soen\Http\Server\Middleware\Middleware;

class Test2Middlewares extends Middleware
{
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		echo '指定中间件:'.self::class.PHP_EOL;
		return $handler->handle($request);
	}

}
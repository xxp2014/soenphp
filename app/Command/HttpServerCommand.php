<?php


namespace App\Command;

use Soen\Command\Command;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HttpServerCommand extends Command
{
	public $httpConfigDirectory = BASE_PATH;

	protected function configure(){
        $this
            // 命令的名称 （"php console_command" 后面的部分）
            ->setName('http_server')
            // 运行 "php console_command list" 时的简短描述
            ->setDescription('start http_server')
            // 运行命令时使用 "--help" 选项时的完整命令描述
            ->setHelp('This command allow you to create models...');
            // 配置一个参数
//            ->addArgument('name', InputArgument::REQUIRED, 'what\'s model you want to create ?')
            // 配置一个可选参数
//            ->addArgument('optional_argument', InputArgument::OPTIONAL, 'this is a optional argument');
	}

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \ReflectionException
     */
	protected function execute(InputInterface $input, OutputInterface $output){
		context()->getComponent('httpServer')->up();
		return 1;
	}

}

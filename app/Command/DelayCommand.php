<?php


namespace App\Command;

use Soen\Command\Command;
use Soen\Delay\Polling;
use Swoole\Coroutine;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DelayCommand extends Command
{
    protected function configure () {
        $this
            // 命令的名称 （"php console_command" 后面的部分）
            ->setName('delay')
            ->setDescription('开始延迟队列任务')
            ->setHelp('请确认命令参数是否正常');
    }

    protected function  execute(InputInterface $input, OutputInterface $output)
    {
        Coroutine::create(function (){
            $delayer = new Polling(1000);
            $delayer->run();
        });
        return 1;
    }
}
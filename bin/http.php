<?php
require_once __DIR__ . '/../vendor/autoload.php';
(new Symfony\Component\Dotenv\Dotenv(true))->load(__DIR__ . '/../.env');
date_default_timezone_set("PRC");
$webDirectory = dirname(__DIR__, 1);
! defined('BASE_PATH') && define('BASE_PATH', $webDirectory);
$componentsDirectory = BASE_PATH . '/components';
$configArray = require_once BASE_PATH . '/config/config.php';
(new Soen\Container\Application($componentsDirectory,$configArray))->run();

//(new Soen\Command\Main())->run();

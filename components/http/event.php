<?php
return [
    'name'  =>  'event',
    'class' =>  Soen\Event\EventDispatcher::class,
    'propertys'  =>  [
    ],
    'args'  =>  [
        App\Listeners\CommandListener::class,
        App\Listeners\DelayListener::class
    ],
];
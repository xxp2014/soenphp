<?php
return [
    'name'  =>  'redis',
    'class' =>  Soen\Redis\RedisProvider::class,
    'propertys'  =>  [
        'config'    =>  [
            'host'          =>  getenv('REDIS_HOST'),
            'port'          =>  getenv('REDIS_PORT'),
            'database'      =>  getenv('REDIS_DATABASE'),
            'password'      =>  getenv('REDIS_PASSWORD'),
            'timeout'       =>  60
        ]
    ],
    'args'  =>  [
    ],
];
<?php
    return [
        'name'  =>  'log',
        'class' =>  Soen\Log\Logger::class,
        'propertys'  =>  [
            'logDriver'     =>  'monolog',
            'level'         => 'info',
            'handlers'      =>  [
                [
                    'class'    =>  Soen\Log\Handler\RotatingFileResetHandler::class,
                    'args'     =>  [
                        'runtime/logs/app.log',
                        2
                    ]
                ]
            ]
        ],
        'args'  =>  [
            'app_log'
        ],
    ];
<?php
return [
	'name'  =>  'command',
	'class' =>  Soen\Command\CommandProvider::class,
	'propertys'  =>  [
		'commands'  =>[
			App\Command\HttpServerCommand::class,
			App\Command\DelayCommand::class
		]
	],
	'args'  =>  [
	],
];
<?php

use App\Controllers\IndexController;
use Soen\Router\Router;
use Soen\Http\Message\Request;
use Soen\Http\Message\Response;
//Router::addRoute(['get'], '/index', [App\Controllers\IndexController::class, 'index'], function (){
//	return [
//		\App\Middlewares\TestMiddlewares::class,
//		\App\Middlewares\Test1Middlewares::class,
//	];
//});

Router::get('/test1', [IndexController::class,'test1']);
Router::get('/test2', [IndexController::class,'test2']);
Router::get('/test3', [IndexController::class,'test3']);
Router::get('/test4', [IndexController::class,'test4']);

Router::get('/index', [App\Controllers\IndexController::class, 'index'], [
	\App\Middlewares\Test2Middlewares::class,
]);

Router::group(['middle'=>[\App\Middlewares\TestMiddlewares::class]], [
	Router::get('/test/{id:\d}/{?name:\w}', [\App\Controllers\IndexController::class, 'index'], [
//		\App\Middlewares\Test1Middlewares::class,
//		\App\Middlewares\TestMiddlewares::class
	]),
]);